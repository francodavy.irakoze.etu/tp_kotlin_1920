<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link href="/css/header.css" rel="stylesheet" type="text/css">
<link href="/css/content.css" rel="stylesheet" type="text/css">
<link rel="icon" type="image/png" href="https://upload.wikimedia.org/wikipedia/commons/thumb/7/74/Kotlin-logo.svg/1024px-Kotlin-logo.svg.png" />
<nav class="nav-bar">
	<div class="container bar-content">
		<div class="onglet-title">
			<p class="title">
				<a href="/index"><img class="logo" src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/74/Kotlin-logo.svg/1024px-Kotlin-logo.svg.png"/></a>
				TP Kotlin
			</p>
		</div>
		<div class="onglet"><a href="/ex1">Etape 1 :</a><span> Singleton</span></div>
		<div class="onglet"><a href="/ex2">Etape 2 :</a><span> Gestion de Liste</span></div>
		<div class="onglet"><a href="/ex3">Etape 3 :</a><span> File Management</span></div>
		<div class="onglet"><a href="/ex4">Etape 4 :</a><span> Coroutines</span></div>
	</div>
</nav>
<br>